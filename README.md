**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

Chợ tốt việc làm xem tin tuyển dụng và tìm việc làm mới nhất hôm nay
Chợ tốt việc làm là kênh tuyển dụng và tìm việc làm uy tín đăng tin miễn phí không giới hạn - nơi tìm kiếm chia sẻ kiến thức kinh nghiệm viết cv và chọn công việc phù hợp chototvieclam.com

https://chototvieclam.com/
---
https://community.powerbi.com/t5/user/viewprofilepage/user-id/63544
https://community.powerbi.com/
https://www.slideshare.net/saobang257/wwwmuabannhadat-infovn
https://www.provenexpert.com/en-us/muabannhadat/
https://issuu.com/muabannhadat/docs/mediakit_vie
https://github.com/thanhphong3390/muabannhadat/issues
https://thuvienphapluat.vn/page/dangky.aspx?step=2&type=free
https://www.jigsawplanet.com/Muabannhadat
https://www.behance.net/search/projects/?search=muabannhadat&sort=recommended&time=month
https://www.mixcloud.com/muabannhadat/
https://www.scout.org/user/2193296


https://www.facebook.com/chovieclamtot
https://twitter.com/VnVitNguyn16
https://chototvieclamsspace.quora.com/
https://www.quora.com/profile/Ch%E1%BB%A3-t%E1%BB%91t-Vi%E1%BB%87c-L%C3%A0m
https://www.blogger.com/profile/00405671569631450283
https://baovieclamtot.blogspot.com/
https://sites.google.com/view/chototvieclam
https://github.com/chovieclam
https://ameblo.jp/chototvieclam/
https://www.pinterest.com/10vieclamtot/viec-tim-nguoi/
https://issuu.com/chototvieclam
https://blogdy.net/chototvieclam
https://autoitvn.com/members/chototvieclam.18801/
http://caycanh.sangnhuong.com/member.php?u=30748
https://vizi.vn/chototvieclam
https://www.youtube.com/channel/UCs2TuAzG2tN8gVFjCZjh21w
https://glints.com/vn/companies/cho-tot-viec-lam/99464d55-d148-4a76-9bfd-c575da53bcb1
https://qiita.com/chototvieclam
https://timvieclamtot.wordpress.com/
https://catchthemes.com/support-forum/users/chototvieclam/
https://www.goodreads.com/user/show/142355901-ch-t-t
https://www.goodreads.com/chototvieclam
https://ello.co/chototvieclam1
https://vi.gravatar.com/chototvieclam1
https://gitlab.com/chovieclam
https://vimeo.com/chototvieclam
https://trello.com/vieclamnhanh/
https://about.me/chototvieclam
https://bbpress.org/forums/profile/chototvieclam/
https://sketchfab.com/chototvieclam
https://soytecantho.vn/Default.aspx?tabid=1000&ch=2139
https://www.provenexpert.com/chototvieclam/
https://raovat.vn/members/chototvieclam.70800/about

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).